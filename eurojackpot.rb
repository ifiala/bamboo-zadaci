#unos korisnika
puts "Unesite 5 brojeva u intervalu od 1 do 50 odvojenih razmacima"
user_normal = gets.chomp.split

puts "Unesite 2 dodatna broja u intervalu od 1 do 10 odvojenih razmacima"
user_bonus = gets.chomp.split



#prebacivanje korisnickog unosa u integer
user_normal.map!(&:to_i)       
user_bonus.map!(&:to_i)
        

#metoda za izvlacenje
def draw(draw_list, draw_size, lower, upper)
    while draw_list.length < draw_size
        exists = false
        number_drawn = rand(lower..upper)
        draw_list.each{ |item|
          if item == number_drawn
            exists = true
          end
        }
        unless exists
            draw_list << number_drawn
        end
    end
end


guessed_normal = false
guessed_bonus = false
i = 0

#while petlja za izvlačenje
while guessed_normal == false || guessed_bonus == false
    #izvlačenje brojeva
    normal_draw = []
    draw(normal_draw, 5, 1, 50)
    bonus_draw = []
    draw(bonus_draw, 2, 1, 10)

    #usporedbe
    result_normal = user_normal - normal_draw
    if result_normal.length == 0
        guessed_normal = true
    end

    result_bonus = user_bonus - bonus_draw
    if result_bonus.length == 0
        guessed_bonus = true
    end
    

    #brojač
    i += 1

    
end


godine = i/52

i.to_s
godine.to_s

puts "Bilo je potrebno #{i} izvlacenja sto je #{godine} godina"